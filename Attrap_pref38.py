from Attrap_prefdpt import Attrap_prefdpt


class Attrap_pref38(Attrap_prefdpt):

    # Configuration de la préfecture
    hostname = 'https://www.isere.gouv.fr'
    raa_page = f'{hostname}/Publications/RAA-Recueil-des-actes-administratifs'
    full_name = 'Préfecture de l\'Isère'
    short_code = 'pref38'
    timezone = 'Europe/Paris'

    # Configuration des widgets à analyser
    year_regex = '(?:(?:[Rr]ecueils{0,1} des [Aa]ctes [Aa]dministratifs de la [Pp]réfecture de l\'Isère[ -]*)|(?:Année ))([0-9]{4})'
    Attrap_prefdpt.grey_card['regex']['year'] = year_regex
    Attrap_prefdpt.white_card['regex']['year'] = year_regex
    Attrap_prefdpt.white_card['exclude'] = ['Vous recherchez "Le Journal officiel de la République française" ?']

    # On ajoute un widget de menu déroulant
    Attrap_prefdpt.select_widgets.append(
        Attrap_prefdpt.DptSelectWidget(
            'menu_deroulant',
            regex='([0-9]{1,2}[er]{0,1} [a-zéû]* [0-9]{4})',
            css_path='select#-liste-docs',
            type='year-month-day'
        )
    )
